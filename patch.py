#!/usr/bin/env python
import argparse
import sys
import struct
def __parseint(s):
    return int(s, 0)
def patch_get_data(patchType, patchData):
    typeDictionary = {
        'u8': 'B',
        's8': 'b',

        'u16le': '<H',
        'u32le': '<I',
        'u64le': '<Q',
        's16le': '<h',
        's32le': '<i',
        's64le': '<q',

        'u16be': '>H',
        'u32be': '>I',
        'u64be': '>Q',
        's16be': '>h',
        's32be': '>i',
        's64be': '>q',

        'f32le': '<f',
        'f64le': '<d',
        'f32be': '>f',
        'f64be': '>d'
    }
    data = None
    if patchType == 'path':
        with open(patchData, 'rb') as f:
            data = f.read()
    elif patchType == 'ascii':
        data = bytearray(patchData, encoding='ascii')
    elif patchType == 'c-ascii':
        data = bytearray(patchData, encoding='ascii')
        data.extend([0])
    elif patchType == 'pas-ascii-be':
        data = bytearray(struct.pack('>I', len(data)))
        data.extend(bytearray(patchData, encoding='ascii'))
    elif patchType == 'pas-ascii-le':
        data = bytearray(struct.pack('<I', len(data)))
        data.extend(bytearray(patchData, encoding='ascii'))
    elif patchType in typeDictionary:
        fmt = typeDictionary[patchType]
        if patchType[0] == 'f':
            data = bytearray(struct.pack(fmt, float(patchData)))
        else:
            data = bytearray(struct.pack(fmt, int(patchData, 0)))
    else:
        raise Exception('invalid patch type "{}"'.format(patchType))
    return data

def patch(destination, source, offset, patchData, patchType='path', insert=False):
    data = patch_get_data(patchType, patchData)

    src = None
    with open(source, 'rb') as f:
        src = f.read()
    if insert:
        with open(destination, 'ab') as f:
            f.write(src)
            f.seek(offset)
            f.write(data)
    else:
        with open(destination, 'wb') as f:
            f.write(src)
            f.seek(offset)
            f.write(data)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Patch a binary file.')
    parser.add_argument('--patchtype', default='path', choices=[
        'path', 
        'ascii', 'c-ascii', 'pas-ascii-le', 'pas-ascii-be',
        'u8',    's8',
        'u16le', 'u16be', 's16le', 's16be',
        'u32le', 'u32be', 's32le', 's32be',
        'u64le', 'u64be', 's64le', 's64be',
        'f32le', 'f32be',
        'f64le', 'f64be'
    ])
    parser.add_argument('--insert', default=False, action='store_true')
    parser.add_argument('destination', type=str)
    parser.add_argument('source', type=str)
    parser.add_argument('patch', type=str)
    parser.add_argument('offset', type=__parseint)

    args = parser.parse_args()
    patch(args.destination, args.source, args.offset, args.patch, args.patchtype, args.insert)
